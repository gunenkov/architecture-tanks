﻿using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;

namespace TanksGame.Commands.Move
{
    /// <summary>
    /// Команда движения
    /// </summary>
    public class MoveCommand : ICommand
    {
        private IMovable movable;

        public MoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            movable.Position += movable.Velocity;
            IoC.Resolve<object>("queue.add", this);
        }
    }
}
