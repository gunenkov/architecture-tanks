﻿using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;

namespace TanksGame.Commands.Move
{
    public class StartMoveCommand : ICommand
    {
        private IMovable movable;
        public StartMoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            var moveCommand = new MoveCommand(movable);
            IoC.Resolve<object>("queue.add", moveCommand);
            movable.SetCommand(moveCommand);
        }
    }
}
