﻿using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;

namespace TanksGame.Commands.Move
{
    public class StopMoveCommand : ICommand
    {
        private IMovable movable;
        public StopMoveCommand(IMovable movable)
        {
            this.movable = movable;
        }

        public void Execute()
        {
            IoC.Resolve<object>("queue.remove", movable.GetCommand());
            movable.RemoveCurrentCommand();
        }
    }
}
