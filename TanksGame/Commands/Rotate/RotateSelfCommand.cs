﻿using System;
using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;
using Utils.Numeric;
using Utils.Numeric.Extensions;

namespace TanksGame.Commands.Rotate
{
    /// <summary>
    /// Комманда поворота вокруг собственной оси (изменение направления)
    /// </summary>
    public class RotateSelfCommand : ICommand
    {
        private IRotatable rotatable;

        public RotateSelfCommand(IRotatable rotatable)
        {
            this.rotatable = rotatable;
        }

        public void Execute()
        {
            var currentDirection = rotatable.Direction;
            rotatable.Direction = new Vector(
                (int)Math.Round(currentDirection.GetNComponent(1) * Math.Cos(rotatable.AngularVelocity.ToRadians())) 
                - (int)Math.Round(currentDirection.GetNComponent(2) * Math.Sin(rotatable.AngularVelocity.ToRadians())),
                (int)Math.Round(currentDirection.GetNComponent(2) * Math.Cos(rotatable.AngularVelocity.ToRadians()))
                + (int)Math.Round(currentDirection.GetNComponent(1) * Math.Sin(rotatable.AngularVelocity.ToRadians()))
            );
            IoC.Resolve<object>("queue.add", this);
        }
    }
}
