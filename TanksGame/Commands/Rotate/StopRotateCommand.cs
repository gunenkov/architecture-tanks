﻿using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;

namespace TanksGame.Commands.Rotate
{
    public class StopRotateCommand : ICommand
    {
        private IMoveRotatable moveRotatable;
        public StopRotateCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            IoC.Resolve<object>("queue.remove", moveRotatable.GetCommand());
            moveRotatable.RemoveCurrentCommand();
        }
    }
}
