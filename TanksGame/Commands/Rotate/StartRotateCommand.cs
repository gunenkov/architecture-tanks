﻿using TanksGame.Base;
using TanksGame.Contracts;

namespace TanksGame.Commands.Rotate
{
    public class StartRotateCommand
    {
        private IMoveRotatable moveRotatable;
        public StartRotateCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            var rotateMacroCommand = new RotateMacroCommand(moveRotatable);
            IoC.Resolve<object>("queue.add", rotateMacroCommand);
            moveRotatable.SetCommand(rotateMacroCommand);
        }
    }
}
