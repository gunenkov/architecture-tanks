﻿using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;

namespace TanksGame.Commands.Rotate
{
    public class RotateMacroCommand : ICommand
    {
        private IMoveRotatable moveRotatable;

        public RotateMacroCommand(IMoveRotatable moveRotatable)
        {
            this.moveRotatable = moveRotatable;
        }

        public void Execute()
        {
            new RotateSelfCommand(moveRotatable).Execute();
            new RotateVelocityCommand(moveRotatable).Execute();
        }
    }
}
