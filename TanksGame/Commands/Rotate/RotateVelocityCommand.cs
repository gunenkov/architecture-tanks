﻿using System;
using TanksGame.Base;
using TanksGame.Commands.Abstractions;
using TanksGame.Contracts;
using Utils.Numeric;
using Utils.Numeric.Extensions;

namespace TanksGame.Commands.Rotate
{
    public class RotateVelocityCommand : ICommand
    {
        IMoveRotatable moveRotateable;

        public RotateVelocityCommand(IMoveRotatable moveRotateable)
        {
            this.moveRotateable = moveRotateable;
        }

        public void Execute()
        {
            var currentVelocity = moveRotateable.Velocity;
            moveRotateable.Velocity = new Vector(
                (int)Math.Round(currentVelocity.GetNComponent(1) * Math.Cos(moveRotateable.AngularVelocity.ToRadians()))
                - (int)Math.Round(currentVelocity.GetNComponent(2) * Math.Sin(moveRotateable.AngularVelocity.ToRadians())),
                (int)Math.Round(currentVelocity.GetNComponent(2) * Math.Cos(moveRotateable.AngularVelocity.ToRadians()))
                + (int)Math.Round(currentVelocity.GetNComponent(1) * Math.Sin(moveRotateable.AngularVelocity.ToRadians()))
            );
            IoC.Resolve<object>("queue.add", this);
        }
    }
}
