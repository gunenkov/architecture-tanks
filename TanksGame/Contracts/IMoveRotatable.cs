﻿namespace TanksGame.Contracts
{
    public interface IMoveRotatable : IManageable, IRotatable, IMovable
    {
    }
}
